Breakpoint.init({
    xs: {
        min: 0,
        max: 480
    },
    sm: {
        min: 480,
        max: 987
    },
    md: {
        min: 987,
        max: 1628
    },
    lg: {
        min: 1628,
        max: 9999
    }
});

// $(window).on('change:breakpoint', function (e, current, previous) {
//     console.log('previous breakpoint was', previous);
//     console.log('current breakpoint is', current);
// });

(function($){
    let $dlmenu = $( '#dl-menu' );
    if( $dlmenu.length ) {
        $dlmenu.dlmenu({
            animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
        });
    }
}(jQuery));

/**
 * data-href JS redirect
 */
(function($){

  let $link = $('[data-href]');

  $link.on('click',function(){
    window.location = $(this).data('href');
  });

}(jQuery));

/**
 * uzsakyti-prenumerata.html form
 */
(function($){
    let $plan = $('.plan');

    $plan.on('click',function(e){
        e.preventDefault();

        let thisPlan = $(this).data('plan');

        $('input[name=plan]').remove();
        $plan.removeClass('active');

        $(this).addClass('active');
        $(this).after('<input type="hidden" name="plan" value="'+thisPlan+'">');
    })
}(jQuery));

// ------------------------------------------------------
// Trigger ideti patarima menu
// ------------------------------------------------------
(function($){
  let $trigger = $('.ideti_patarima');
  let $main_menu = $('.header-menus > div:first-child');
  let $ideti_menu = $('.header-menus > div:last-child');

  $trigger.on('click', function(e){
    e.preventDefault();

    $main_menu.fadeToggle('fast');
    $ideti_menu.fadeToggle('fast');

  });

}(jQuery));

// ------------------------------------------------------
// Toggler class
// ------------------------------------------------------
(function($){
    $(function(){
        $(document).on("click","[data-toggleclass]",function(e) {
            e.preventDefault();

            if ( $(this).hasClass('active') ) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        })
    })
}(jQuery));

// ------------------------------------------------------
// Toggler class 2
// ------------------------------------------------------
(function($){
    $(function(){
        $(document).on("click","[data-toggleclass2]",function(e) {
            e.preventDefault();

            $("[data-toggleclass2]").removeClass('active');

            if ( $(this).hasClass('active') ) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        })
    })
}(jQuery));

// ------------------------------------------------------
// Outrights toggler
// ------------------------------------------------------
(function($){
    $("#outrights").on("click touchstart",function(e) {

        let $p = $('.primary');

        $p.fadeOut('fast',function(){
            let $s = $('.secondary');
            $s.fadeIn('fast');
            $s.next('.tab-content').fadeIn('fast');
        });

        $p.next('.tab-content').fadeOut('fast');

        e.preventDefault();

    });
    $("#matches").on("click touchstart",function(e) {

        let $s = $('.secondary');

        $s.fadeOut('fast',function(){
            let $p = $('.primary');
            $p.fadeIn('fast');
            $p.next('.tab-content').fadeIn('fast');
        });

        $s.next('.tab-content').fadeOut('fast');

        e.preventDefault();

    })
}(jQuery));

// ------------------------------------------------------
// Universal toggler
// ------------------------------------------------------
(function($){
    $(function(){
        $(document).on("click touchstart","[data-toggle]",function(e) {

            $( "#" + $(this).data('toggle') ).slideToggle("fast");

            if ( $(this).hasClass('open') ) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }

            e.preventDefault();

        })
    })
}(jQuery));

// ----------------------------------------------------------------------
// Remove statymu lapelis
// ----------------------------------------------------------------------
(function($){

    let $removeBtn = $('.ico-removeaus');

    $removeBtn.on('click',function(e){
       e.preventDefault();

        $(this).parent().parent().slideUp('fast',function(){
            $(this).remove();
        });

        $.ajax({
            type: "POST",
            url: $removeBtn.data('ajax'),
            id: $removeBtn.data('id'),
            dataType: 'JSON'
        })
    });

}(jQuery));

/**
 * Toggle komentarai
 */
(function($){
    $(document).on("click touchstart","a[data-togglecomments]",function(e) {
        e.preventDefault();

        let comm = $(this).data('togglecomments');

        $( "#" + $(this).data('togglecomments') ).slideToggle( "fast", function() {
            // Animation complete.
        });

    })
}(jQuery));

(function($){
    let $validation = $('.validation');

    if($validation.length) {
        $validation.validate({
            lang: 'lt'
        });
    }
}(jQuery));

/**
 * Select2
 */
(function($){
    $(function(){

        $('.select2').select2({
            minimumResultsForSearch: 5
        });

        function formatC(c) {
            if (!c.id) { return c.text; }
            return $(
                '<span><img src="assets/img/flags/' + c.element.value.toLowerCase() + '.png" class="img-flag" /> ' + c.text + '</span>'
            );
        }

        $('#salis').select2({
            placeholder: "Pasirinkite šalį",
            templateSelection: formatC,
            templateResult: formatC,
        });

        let $statsSelect = $('#stats');

        $('#stats_'+$statsSelect.val()).css('display','flex');

        let statsAfter = $statsSelect.on('select2:select');

        $statsSelect.on('select2:selecting', function(e){
          let statsBefore = $(this).val();
          $('#stats_'+statsBefore).slideUp('fast',function(){

            $('#stats_'+statsAfter.val()).css('display','flex');
            $('#stats_'+statsAfter.val()).slideDown('fast',function(){
              $(this).css('display','flex');
            });
            
          });
        });

        // Ajax load
        let $dataToLoad = $('[data-sload][data-sload!=""]');
        $dataToLoad.select2({
          language: "lt",
          ajax: {
              url: $dataToLoad.data('sload'),
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term, // search term
                  page: params.page
                };
              },
              processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                  results: data.items,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: false
          },
          escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 1,
          templateResult: formatUsers,
          templateSelection: formatUserSelection
        });

        function formatUserSelection (user) {
          return user.name || user.text;
        }
        function formatUsers (users) {
          if (users.loading) return users.name;
          let markup = users.name;
          return markup;
        }

    });
}(jQuery));

/**
 * Section 3 dropdown
 */
(function($){

    $(".dropdown-menu li a").click(function(e){
        $(this).parents(".dropdown").find('.btn').html( '<span>' + $(this).html() + '</span><span class="caret"></span>' );
        $(this).parents(".dropdown").find('.btn').val( $(this).data('value') );

        e.preventDefault();
    });

}(jQuery));

// --------------------------------------------------
// mano-pardavimai-ikelti.html Periodas change
// --------------------------------------------------
(function($){
    $(function(){

        let $selectPeriodas = $('#periodas');

        $selectPeriodas.on('change', function () {
            $(this).parents('form:first').submit();
        });
    });
}(jQuery));

// --------------------------------------------------
// mano-pardavimai-ikelti.html DataTables
// --------------------------------------------------
(function($){
    $(function(){

        let $table = $('#aktyvus_vartotojai');

        if($table.length) {
            $table.DataTable({
                "paging":false,
                "info":false,
                "searching":false,
                responsive: true
            });
        }

    });
}(jQuery));

/**
 * History back
 */
(function($){
    $("#go_back").on("click", function(e){
        e.preventDefault();
        window.history.back();
    })
}(jQuery));

/**
 * Home Section 3 slider
 */
(function($){
    $(function(){
        new Swiper('.home .swiper-container', {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            slidesPerView: 3,
            spaceBetween: 15,
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 'auto',
                    spaceBetween: 5
                }
            }
        });
    })
}(jQuery));

/**
 * Bonusai top swiper
 */
(function($){
    $(function(){

        new Swiper('.bonusas .swiper-container', {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            slidesPerView: 3,
            spaceBetween: 15,
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 'auto',
                    spaceBetween: 5
                }
            }
        });

    })
}(jQuery));

/**
 * Charts
 */
(function($){
    $(function(){

        let $chart = $('.chart');
        let defaultPColor = '#ca3c3f';
        let defaultCColor = 'rgba(255, 255, 255, 0)';

        if($chart.length) {
            Highcharts.setOptions({
                chart: {
                    type: 'area',
                    backgroundColor: defaultCColor,
                    height: '80'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    allowDecimals: false,
                    labels: false,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels: false,
                    tickLength: 0,
                    crosshair: false,
                    lineWidth: 0,
                    gridLineWidth:0
                },
                tooltip: {
                    formatter: function() {
                        return 'Data: ' + this.point.data + '<br>' + 'Pelnas: ' + this.y
                    }
                },
                plotOptions: {
                    area: {
                        color: defaultPColor
                    }
                }
            });

            let chart = $('.chart > div');

            chart.each(function(){

                let userId = $(this).attr('id');
                let customPColor = $(this).data('plotcolor');

                $.getJSON( $('#'+userId).data('userdata') , function (data){
                    new Highcharts.Chart({
                        chart:{
                            renderTo:userId,
                        },
                        series: [{
                            showInLegend: false,
                            name: '',
                            data: data
                        }],
                        plotOptions: {
                            area: {
                                color: customPColor
                            }
                        }
                    });
                });

            });

        }

    });
}(jQuery));

// -------------------------------------------------------
// Plan subscripbe
// -------------------------------------------------------
(function($){

    let $sform = $('#plan_subscribe');

    $('button[data-plan]').on('click',function(e){
        e.preventDefault();

        $('button[data-plan]').removeClass('active');
        $(this).addClass('active');

        let plan = $(this).data('plan');

        $('.activeplan').remove();
        $sform.append('<input type="hidden" class="activeplan" name="'+plan+'" value="1">');

    });

}(jQuery));

// -------------------------------------------------------
// Offcanvas
// -------------------------------------------------------
if ( Breakpoint.is('xs') || Breakpoint.is('sm') || Breakpoint.is('md') ) {
    (function ($) {

        let w = '25%';
        let $pannel = $('#panel-offcanvas');

        if(Breakpoint.is('xs')) {
            w = '90%';
        } else if(Breakpoint.is('sm')) {
            w = '63%';
        } else if(Breakpoint.is('md')) {
            w = '35%';
        } else if(Breakpoint.is('lg')) {
            w = '25%';
        }

        $pannel.prependTo("body");

        $pannel.scotchPanel({
            containerSelector: 'body', // As a jQuery Selector
            direction: 'right', // Make it toggle in from the left
            duration: 300, // Speed in ms how fast you want it to be
            transition: 'ease', // CSS3 transition type: linear, ease, ease-in, ease-out, ease-in-out, cubic-bezier(P1x,P1y,P2x,P2y)
            clickSelector: '.sidebar-toggler', // Enables toggling when clicking elements of this class
            distanceX: w, // Size fo the toggle
            enableEscapeKey: true,
            afterPanelOpen: function() {
                console.log('Panel is opened...');
            }
        });

    }(jQuery));
}

// -------------------------------------------------------
// Avatar upload preview
// -------------------------------------------------------
function previewAvatar() {
    let preview = document.getElementById('useravatar_preview');
    let file    = document.querySelector('input[name=useravatar]').files[0];
    let reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.style.backgroundImage = 'url(' + reader.result + ')';
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

// -------------------------------------------------------
// Suggestions
// -------------------------------------------------------
(function($){

    let $country = $('input[name=country]');
    let $countries = $country.data('countries');

    if($country.length) {

        let countries = new Bloodhound({
            datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.name); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: $countries,
                filter: function(list) {
                    return $.map(list, function(item) {
                        return {
                            name: item.name,
                            code: item.code
                        };
                    });
                },
                cache: true
            }
        });

        $country.typeahead({
            name: 'countries',
            hint: true,
            highlight: true,
            minLength: 1
        },{
            source: countries,
            display: function(data) { return data.name },
            templates: {
                empty: ['No data'].join('\n'),
                suggestion: function (data) {
                    return '' +
                        '<div class="autocomplete-item">' +
                        '<div><img src="../assets/img/flags/'+data.code.toLowerCase()+'.png" alt="'+data.code+'"></div>' +
                        '<div>'+data.name+'</div>' +
                        '</div>';
                }
            }
        });

        $country.bind('typeahead:selected', function(obj, datum, name) {

            let selectedCountry = datum.code.toLowerCase();

            $(this).parent().find('.selected-flag').remove();

            $(this).parent().append('<div class="selected-flag"><img src="../assets/img/flags/'+selectedCountry+'.png" alt="'+selectedCountry+'"></div>');

            $(this).css('padding-left',65);

        });

    }

}(jQuery));


/**
 * Section 2 tabs
 */
(function($){

    let $tabTrigger = $('.tabs a');

    $tabTrigger.on('click',function(e){

        let hash = $(this).attr('href');

        $('.tab-content').removeClass('opened');

        $('[data-tabcontent="' + hash + '"]' ).toggleClass('opened');

        e.preventDefault()

    });

}(jQuery));

(function($){

    let $hasSubmenu = $('.header-menu .has_submenu');
    let $submenu = $('.header-submenu');
    let $closeSubmenu = $('.header-submenu .close');

    $closeSubmenu.on('click', function(e){

        $submenu.removeClass('active');

        // TODO: sitas dar neveikia...
        let $activeParent = $(this).parent().find('.active').attr('id');
        $('#' + $activeParent).removeClass('active');

        e.preventDefault();

    });

    $hasSubmenu.click(function(e){

        $hasSubmenu.removeClass('active');

        $submenu.addClass('active');

        if( !$(this).hasClass('active') ) {

            $(this).addClass('active');

        }

        let submenuItems = $(this).attr('id');

        $('.header-submenu li').removeClass('active');
        $('.' + submenuItems).addClass('active');

        e.preventDefault();

    })

}(jQuery));

if($('.aos').length) {
    AOS.init();
}

// ----------------------------------------------------------------------------------
// Header stuff
// ----------------------------------------------------------------------------------
(function($){
    $(function(){
        $('.smaller').css('display','block');
    });
}(jQuery));

function init() {
    window.addEventListener('scroll', function(e){
        let distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 500,
            $stickymenu = jQuery('.stickymenu');
        if (distanceY > shrinkOn) {
            $stickymenu.addClass('smaller');
        } else {
            if ($stickymenu.hasClass('smaller')) {
                $stickymenu.removeClass('smaller');
            }
        }
    });
}
window.onload = init();