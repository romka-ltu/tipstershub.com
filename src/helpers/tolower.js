module.exports = function(options) {
    if (options.fn(this) && typeof options.fn(this) === 'string') {
        return options.fn(this).toLowerCase();
    }
};